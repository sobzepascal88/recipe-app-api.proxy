# recipe-app-api.proxy

NGINX proxy app for our recipe API

## Usage

### Environment variables

  * `LISTEN_PORT` - Port to listen on (default: `8000`)
  * `APP_PORT` - Host name of the app to forward resquests to (default: `app`)
  * `APP_PORT` - Port of the app to forward requests to (default: `9000`)